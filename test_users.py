from users import User


def test_users_initial_state():
    test_user = User("test@example.com", "passwOrd")
    assert test_user.email == "test@example.com"
    assert test_user.password == "passwOrd"
    assert not test_user.logged_in


def test_succesful_login():
    test_user = User("test@example.com", "passwOrd")
    test_user.login("passwOrd")
    assert test_user.logged_in

def test_unsuccesful_login():
    test_user = User("test@example.com", "passwOrd")
    test_user.login("ijybtfg")
    assert not test_user.logged_in

def test_logout ():
    test_user = User("test@example.com", "passwOrd")
    test_user.login("passwOrd")
    test_user.logout()
    assert not test_user.logged_in

